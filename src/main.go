package main

import "os"
import "./demon"

func main() {
	if (len(os.Args) < 2) {
		panic("no action");
	}
	var action string = os.Args[1];
	switch(action){
	case "run":
		if (len(os.Args) < 3) {
			panic("missed run type param");
		}
		if (len(os.Args) < 4) {
			panic("missed configuration file param");
		}
		switch os.Args[2] {
		case "apiResponser":
			demon.RunApiResponser(os.Args[3]);
		case "instance":
			demon.RunInstance(os.Args[3]);
		default:
			panic("can run 'server' or 'instance' only");
		}

	default:
		panic("illegal action")
	}
}
