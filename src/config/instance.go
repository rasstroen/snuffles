package config

var Instance InstanceConfig;

type InstanceConfig struct {
	Id   int64
	Host string
	Port int64
	Size int64
}

func PrepareInstanceConfig(ConfigFilePath string) {
	Instance = ReadInstanceConfig(ConfigFilePath + "instance.conf");
}