package config

var Cluster ClusterConfig;

type ClusterConfig struct {
	Replication struct {
			    meta  int64
			    files int64
		    }
}

func PrepareClusterConfig(ConfigFilePath string) {
	Cluster = ReadClusterConfig(ConfigFilePath + "cluster.conf");
}
