package config


import "os"
import "github.com/BurntSushi/toml"

func ReadClusterConfig(filePath string) ClusterConfig {
	_, err := os.Stat(filePath)
	if err != nil {
		panic("Config file is missing: " + filePath)
	}

	var config ClusterConfig
	if _, err := toml.DecodeFile(filePath, &config); err != nil {
		panic(err)
	}
	return config
}

func ReadInstanceConfig(filePath string) InstanceConfig {
	_, err := os.Stat(filePath)
	if err != nil {
		panic("Config file is missing: " + filePath)
	}

	var config InstanceConfig
	if _, err := toml.DecodeFile(filePath, &config); err != nil {
		panic(err)
	}
	return config
}
