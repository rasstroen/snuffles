package demon

import "../config"
import "./instance"

func RunInstance(configFilePath string) {
	// prepare cluster configuration
	config.PrepareClusterConfig(configFilePath)
	// prepare instance configuration
	config.PrepareInstanceConfig(configFilePath)
	// running demons
	go intstance.MetaDemon(true);
	go intstance.AttrsDemon(true);
	go intstance.FilesDemon(true);
	for {
		intstance.StatusDemon(false);
	}
}
