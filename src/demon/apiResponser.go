package demon

import "fmt"
import "../config"

func RunApiResponser(configFilePath string) {
	config.PrepareClusterConfig(configFilePath)
	config.PrepareInstanceConfig(configFilePath)
	fmt.Println(config.Cluster)
}